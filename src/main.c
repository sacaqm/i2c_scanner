#include <nrf9160.h>
#include <zephyr/sys/printk.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/gpio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define I2C_NODE DT_NODELABEL(i2c2)
#

void main(void)
{
	
	k_sleep(K_SECONDS(1));

	printk("Starting i2c scanner...\n");

	const struct device * i2c_dev = DEVICE_DT_GET(I2C_NODE);
	
	if(!device_is_ready(i2c_dev)){
		printk("I2C: Not ready\n");
	}else{
		printk("I2C: Ready!!!\n");
	}
	
	uint8_t error = 0u;
	
	i2c_configure(i2c_dev, I2C_SPEED_SET(I2C_SPEED_STANDARD));

	for (uint8_t i = 4; i <= 0x77; i++) {
		printk("Scan address: 0x%2x\n",i);
		struct i2c_msg msgs[1];
		uint8_t dst = 1;

		/* Send the address to read from */
		msgs[0].buf = &dst;
		msgs[0].len = 1U;
		msgs[0].flags = I2C_MSG_WRITE | I2C_MSG_STOP;
		
		error = i2c_transfer(i2c_dev, &msgs[0], 1, i);
		if (error == 0) {
			printk("=> Device found !!!!!!!\n");
		}else {
			printk("=> Error: %d \n", error);
		}
		
		
	}
	printk("Scanning done\n");
	

	
	
}